const basicAuth = require('express-basic-auth')
const {usersGet} = require('../models/users.model')

function verifyAdmin(req, res, next) {
    const auth = decode(req.headers.authorization);
    if (authentication(auth[0],auth[1])){
        usersGet().find(u =>( basicAuth.safeCompare(auth[0], u.email) && u.isAdmin)) ? next() : res.sendStatus(403)
    }

}

function decode(authHeader) {
    return Buffer.from(authHeader.split(" ")[1], 'base64').toString('ascii').split(':');
}

function authentication(email, pass) {
    const user = usersGet().filter(u =>(basicAuth.safeCompare(email.toString(), u.email.toString()) && basicAuth.safeCompare(pass.toString(), u.password.toString())))[0]
    return user ? true : false
}

function verifyUser(req, res, next) {
    const auth = decode(req.headers.authorization);
    authentication(auth[0],auth[1]) ? next() : res.sendStatus(403);
}

function getUserId (headers) {
    const emailUSer = decode(headers);
    return usersGet().find(u =>(basicAuth.safeCompare(emailUSer[0].toString(), u.email.toString()) && basicAuth.safeCompare(emailUSer[1].toString(), u.password.toString()))).id;
}

module.exports = {verifyAdmin, verifyUser, getUserId}


