const {
    getProducts
} = require('./products.model')
const {
    payMethodsGet
} = require('./payment.model')
const {
    usersGet
} = require('./users.model')

const estados = [
    "NUEVO", "CONFIRMADO", "PREPARANDO", "ENVIADO", "CANCELADO", "ENTREGADO"
]

const total = (productos) => {
    const suma = (a, b) => {
        return (getProducts().find(e => e.id == b.id).price * b.cantidad) + a;
    }
    x = (productos.reduce(suma, 0));
    return x;
}

const pedido = [{
        "id": 1,
        "id_usuario": 3,
        "productos": [{
            "id": 2,
            "cantidad": 1,
        }],
        "total": 0,
        "direccion": "av 3",
        "metodo_pago": 1,
        "status": "NUEVO",
    },
    {
        "id": 2,
        "id_usuario": 1,
        "productos": [{
            "id": 2,
            "cantidad": 1,
        }],
        "total": 0,
        "direccion": "av 3",
        "status": "NUEVO",
        "metodo_pago": 1,

    },
    {
        "id": 3,
        "id_usuario": 2,
        "productos": [{
            "id": 2,
            "cantidad": 3
        }],
        "total": 0,
        "direccion": "av 3",
        "metodo_pago": 1,
        "status": "NUEVO",
    }
];

const getPedidos = () => {
    return pedido.map(e => {
        e.total = total(e.productos);
        e.productos = e.productos.map(u => {
            u.name = getProducts().find(x => x.id == u.id).name
            u.price = getProducts().find(x => x.id == u.id).price
            return u
        })

        // e.metodo_pago_nombre = payMethodsGet().find(x => x.id == e.metodo_pago).method
        return e
    })
}

const createPedidos = (id_usuario, productos, direccion, metodo_pago) => {
    mayor = 0;
    for (i in pedido) {
        if (pedido[i].id > mayor) {
            mayor = pedido[i].id;
        }
    }
    const direccion_pedido = direccion ? direccion : usersGet().filter(u => u.id === id_usuario).direccion;

    const total_pedido = total(productos)
    pedido.push({
        "id": mayor + 1,
        "id_usuario": id_usuario,
        "productos": productos,
        "total": total_pedido,
        "direccion": direccion_pedido,
        "metodo_pago": payMethodsGet().filter(u => u.id === metodo_pago),
        "status": "NUEVO"
    });
    return pedido.filter(e => e.id === mayor + 1);
};

const confirmarPedido = (id_pedido) => {
    const indPedido = pedido.findIndex(e => e.id === id_pedido);
    if (indPedido >= 0) {
        pedido[indPedido].status = "CONFIRMADO";
        return pedido[indPedido]
    } else {
        return false;
    }

}

const addProductos = (id_pedido, productosNuevos) => {
    const pedidoV = pedido.findIndex(u => u.id === id_pedido);
    if (pedido[pedidoV].status != "NUEVO") {
        return false;
    } else {
        pedido[pedidoV].productos = pedido[pedidoV].productos.concat(productosNuevos)
        productosNuevos.forEach(u => pedido[pedidoV].productos.append(u))
        return pedido[pedidoV];
    }
}

const editPedido = (id_pedido, productosNuevos, direccionNueva, metodo_pagoNuevo) => {
    const pedidoV = pedido.findIndex(u => u.id === id_pedido);
    if (pedido[pedidoV].status != "NUEVO") {
        return false;
    } else {
        if (productosNuevos) {
            pedido[pedidoV].productos = pedido[pedidoV].productos.concat(productosNuevos)
            // productosNuevos.forEach(u => {pedido[pedidoV].productos.append(u);return 1})
        }
        if (direccionNueva) {
            pedido[pedidoV].direccion = direccionNueva
        }
        if (metodo_pagoNuevo) {
            pedido[pedidoV].metodo_pago = metodo_pagoNuevo
        }
        return pedido[pedidoV];
    }
}
const changeStatusPedido = (id_pedido, statusNuevo) => {
    const pedidoV = pedido.findIndex(u => u.id === id_pedido);
    if (pedidoV < 0) {
        return false;
    } else {
        pedido[pedidoV].status = statusNuevo
        return pedido[pedidoV];
    }
}
module.exports = {
    getPedidos,
    createPedidos,
    confirmarPedido,
    addProductos,
    editPedido,
    changeStatusPedido
};


