const products = [
    {
        id: 1,
        name: "Bagel de salmon",
        price: 425,
        activo: true
    },
    {
        id: 2,
        name: "Hamburguesa clasica",
        price: 350,
        activo:true
    },
    {
        id: 3,
        name: "Sandwich veggie",
        price: 310,
        activo:true
    },
    {
        id: 4,
        name: "Ensalada veggie",
        price: 340,
        activo:true
    }
]

const getProducts = () => {
    return products;
}

const listProducts = () => {
    return products.filter(p => p.activo);
}

const createProducts = (name, price) => {
    mayor=0
    for (i in products){
        if (products[i].id>=mayor){
            mayor=products[i].id
        }
    }
    console.log(name);
    products.push({"id":mayor+1, "name":name, "price":price, "activo":true});
    return ({"id":mayor+1, "name":name, "price":price, "activo":true})
}

const updateProducts = (id, name, price, activo) => {
    findId = products.findIndex(i => i.id === id);
    products[findId].name = name;
    products[findId].price = price;
    products[findId].activo = activo;
    return{
        "id":findId,
        "name":name,
        "price" : price,
        "activo":activo
    }
}

const deleteProducts = (id) => {
    deleteId = products.findIndex(u => u.id==id);
    console.log(deleteId);
    if (deleteId >= 0){
        products.splice(deleteId,1);
        return true
    }else{
        return false
    }
}

module.exports = {
    getProducts,
    createProducts,
    updateProducts,
    deleteProducts,
    listProducts
};


