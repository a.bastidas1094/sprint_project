const payMethod = [
    {
        id: 1,
        method: "efectivo"
    },
    {
        id: 2,
        method: "tarjeta de crédito"
    },
    {
        id: 3,
        method: "tarjeta debito"
    },
    {
        id: 4,
        method: "nequi"
    }
];

const payMethodsGet = () => {
    return payMethod;
}
const createPayMethod = (method) => {
    mayor=0
    for (i in payMethod){
        if (payMethod[i].id>=mayor){
            mayor=payMethod[i].id
        }
    }
    payMethod.push({"id":mayor+1, "method":method});
    return ({"id":mayor+1, "method":method})
};
const deletePayMethod = (id) => {
    deleteId = payMethod.findIndex(e => e.id  === id);
    if (deleteId>=0){
        payMethod.splice(deleteId,1);
        return true
    }else{
        return false
    }
};
const editPayMethod = (id,method) => {
    findId = payMethod.findIndex(e => e.id === id);
    if (findId>=0){
        payMethod[findId].method=method;
        return {
            "id":id,
            "Metodo de pago":method
            }
    }else{
        return false
    }
};


module.exports = {payMethodsGet,createPayMethod,deletePayMethod,editPayMethod};


