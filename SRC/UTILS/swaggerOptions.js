const swaaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'projectsprint1',
            version: '1.0.0',
        },
        servers: [
            {
                url: "http://localhost:3000/api-docs/",
                description: "Servidor local"
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security: [
            {
                basicAuth: []
            }
        ]
    },
    apis: ['./src/routes/*.js'], 
};


module.exports = swaaggerOptions;


