const express = require('express');
const pedidosRouter = express.Router();

const {
    verifyAdmin,
    verifyUser,
    getUserId
} = require('../middlewares/auth');
const {
    getPedidos,
    createPedidos,
    confirmarPedido,
    addProductos,
    editPedido,
    changeStatusPedido
} = require('../models/pedidos.model');


/**
 * @swagger
 * /getPedidos:
 *  get:
 *      summary: Obtener la lista de pedidos con credenciales de admin
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: JSON con lista de pedidos                       
 */

pedidosRouter.get('/getPedidos', verifyAdmin, (req, res) => {
    res.json(getPedidos());
});


/**
 * @swagger
 * /listPedidos:
 *  get:
 *      summary: Obtener la lista de pedidos con credenciales de usuario
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: JSON con pedis
 *              
 *                          
 */

pedidosRouter.get('/listPedidos', verifyUser, (req, res) => {
    const id_usuario = getUserId(req.headers.authorization);
    const pedidos = getPedidos().filter(e => e.id_usuario == id_usuario);
    res.status(200).json({
        "listado de pedidos": pedidos
    });
});


/**
 *@swagger
 * /createPedido:
 *  post:
 *      summary: Crear nuevo pedido
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          example:
 *                              productos: [{"id": 1, "cantidad": 4}]
 *                              direccion: av56 mil45
 *                              metodo_pago: 1
 *      responses:
 *          '200':
 *              description: pedido creado
 *              
 *          '400':
 *              description: campos incompletos, llena todos los campos requeriodos
 *                  
 */

pedidosRouter.post('/createPedido', verifyUser, (req, res) => {
    const {
        productos,
        direccion,
        metodo_pago
    } = req.body;
    const id_usuario = getUserId(req.headers.authorization);
    if (id_usuario && productos && direccion && metodo_pago) {
        const pedido_creado = createPedidos(id_usuario, productos, direccion, metodo_pago);
        res.json({
            "Pedido creado": pedido_creado
        });
    } else {
        res.status(400).json({
            "Campos incompletos": "Llena todos los campos requeridos"
        });
    }
});

/**
 *@swagger
 * /confirmPedido:
 *  post:
 *      summary: Confirmar pedido
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          example:
 *                              id_pedido: 5
 *      responses:
 *          '200':
 *              description: pedido confirmado
 *          '400':
 *              description: Error, el pedido no se ha encontrado
 *                  
 */
pedidosRouter.post('/confirmPedido', verifyUser, (req, res) => {
    const {
        id_pedido
    } = req.body;
    const id_usuario = getUserId(req.headers.authorization);
    console.log(id_usuario);
    const pedido = getPedidos().find(u => u.id === id_pedido && u.id_usuario == id_usuario && u.status == "NUEVO")
    if (pedido) {
        pedConf = confirmarPedido(id_pedido);
        if (pedConf) {
            res.json({
                "Pedido confirmado": pedConf
            })
        } else {
            res.status(404).json({
                "Error": "Pedido no encontrado"
            });

        }
    } else {
        res.status(404).json({
            "Error": "pedido no encontrado o ya está confirmado"
        });
    }
});
/**
 * @swagger
 * /addPedido:
 *  put:
 *      summary: añadir productos al pedido mientras no este confirmado
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      example:
 *                          id_pedido: 3
 *                          productos: [{"id": 2, "cantidad": 3}]
 *      responses:
 *          '200':
 *              description: Productos agragados exitosamente al carrito
 *              
 *          '404':
 *              description: Error, pedido no encontado o ya está confirmado
 */

pedidosRouter.put('/addPedido', verifyUser, (req, res) => {
    const {
        id_pedido,
        productos
    } = req.body;
    const id_usuario = getUserId(req.headers.authorization);
    const pedido = getPedidos().find(u => u.id === id_pedido && u.id_usuario == id_usuario && u.status == "NUEVO");
    if (pedido) {
        pedConf = addProductos(id_pedido, productos);
        if (pedConf) {
            res.json({
                "Productos agregados al carrito": getPedidos().find(u => u.id === id_pedido)
            })
        } else {
            res.status(404).json({
                "Error": "Pedido no encontrado"
            });

        }
    } else {
        res.status(404).json({
            "Error": "pedido no encontrado o ya está confirmado"
        });
    }
});

/**
 * @swagger
 * /editPedido:
 *  put:
 *      summary: Editar productos, direccion o metodo de pago mientras no este confirmado
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      example:
 *                          id_pedido: 3
 *                          productos: [{"id": 2, "cantidad": 3}]
 *                          direccion: "por la casa mia"
 *                          metodo_pago: 2
 *      responses:
 *          '200':
 *              description: pedido editado
 *              
 *          '404':
 *              description: Error, pedido no encontrado
 */
pedidosRouter.put('/editPedido', verifyUser, (req, res) => {
    const {
        id_pedido,
        productos,
        direccion,
        metodo_pago
    } = req.body;
    const id_usuario = getUserId(req.headers.authorization);
    const pedido = getPedidos().find(u => u.id === id_pedido && u.id_usuario == id_usuario && u.status == "NUEVO");
    if (pedido) {
        pedConf = editPedido(id_pedido, productos, direccion, metodo_pago);
        if (pedConf) {
            res.json({
                "Productos agregados al carrito": pedConf
            })
        } else {
            res.status(404).json({
                "Error": "Pedido no encontrado"
            });

        }
    } else {
        res.status(404).json({
            "Error": "pedido no encontrado o ya está confirmado"
        });
    }
});

/**
 * @swagger
 * /changeStatusPedido:
 *  put:
 *      summary: editar estado de pedido con credenciales de admin
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      example:
 *                          id_pedido: 2
 *                          status: preparando
 *      responses:
 *          '200':
 *              description: Método de pago editado
 *              
 *          '400':
 *              description: Error, no se encuentra el método de pago
 */
pedidosRouter.put('/changeStatusPedido', verifyAdmin, (req, res) => {
    const {
        id_pedido,
        status
    } = req.body;
    const pedido = getPedidos().find(u => u.id === id_pedido)
    if (pedido) {
        pedConf = changeStatusPedido(id_pedido, status);
        if (pedConf) {
            res.json({
                "Estado del pedido modificado": pedConf
            })
        } else {
            res.status(404).json({
                "Error": "Pedido no encontrado"
            });

        }
    } else {
        res.status(404).json({
            "Error": "Pedido no encontrado o ya está confirmado"
        });
    }
});

module.exports = {
    pedidosRouter
};


