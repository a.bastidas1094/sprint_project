const express = require('express');
const basicAuth = require('express-basic-auth');

const usuarioRouter = express.Router();

const{ usersAdd, usersGet } = require('../models/users.model');

/**
 * @swagger
 * paths:
 *  /create:
 *      post:
 *          summary: Crea nuevo usuario
 *          security: []
 *          tags: [Usuarios] 
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                              email: 
 *                                  type: string
 *                              password:
 *                                  type: string
 *                              phoneNumber:
 *                                  type: string
 *                              address:
 *                                  type: string
 *                          example:
 *                              name: nureidys
 *                              email: nureidys@nada.com
 *                              password: '1234567890'
 *                              phoneNumber: '3204567743'
 *                              address: 'av 23 iiddv'
 *          
 *          responses:
 *              '201':
 *                  description: Usuario creado
 *                  content:
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 *              '400': 
 *                  description: Requerimiento errado
 *                  content: 
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties: 
 *                                  err:
 *                                      type: string     
 *                                        
 */
usuarioRouter.post('/create', (req,res) => {
    const {name,email,password,phoneNumber,address} = req.body;
    console.log(req.auth);
    // console.log(email.search(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3,4})+$/))
    if (name && email && password && phoneNumber && address){
        if(email.search(/\w+@\w+/) >= 0 && password.length > 3){
            if (checkEmail(email)){
                res.status(201).json({
                    "status":"usuario creado",
                    "usuario":{
                        "id":userID(),
                        "name":name,
                        "email":email
                    }
                });
                usersAdd(userID(),name.toString(),email.toString(),password.toString(),phoneNumber.toString(),address);
                console.log(usersGet())
            }else{
                res.status(400).json({"ERROR": "Este correo ya existe"});
            }  
            
        }else{
            res.status(400).json("Debes crear una cuenta email completa y contraseña mayor a 3 digitos")
        }
    }else{
        res.status(400).json("completa todos los campos requeridos");
    }
    
});

/**
 * @swagger
 * paths:
 *  /login:
 *      post:
 *          summary: Login de usuario antes registrado
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email: 
 *                                  type: string
 *                              password: 
 *                                  type: string
 *                          example: 
 *                              email: alicia@nada.com
 *                              pass: '4321'
 *          tags: [Usuarios]
 *          responses:
 *              '201':
 *                  description: Logueado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 *              '400':
 *                  description: Email o contraseña incorrectos
 * 
 */
usuarioRouter.post('/login',(req,res) => {
    const{email,password} = req.body;
    const userBD = usersGet().filter(u =>(basicAuth.safeCompare(email, u.email) && basicAuth.safeCompare(password, u.password)))[0]
    if (userBD){
        res.status(201).json({"EXITO": "Usuario logueado","usuario":{
            "id":userBD.id,
            "name":userBD.name,
            "email":userBD.email
        }})
    }else{
        res.status(400).json({"ERROR":"Email o contraseña incorrectos"})
    }
});

function checkEmail (email){
    partEqual = usersGet().find(u=> u.email === email);

    if (!partEqual){
        return true;
    }else{
        return false;
    }
}

function userID () {
    mayor=0
    for (i in usersGet()){
        if (usersGet()[i].id>=mayor){
            mayor=usersGet()[i].id + 1;
        }
    }
    return mayor;
}


module.exports = {usuarioRouter };

