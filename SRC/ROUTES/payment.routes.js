const express = require('express');
const paymentsRouter = express.Router();

const {payMethodsGet,createPayMethod,deletePayMethod,editPayMethod} = require('../models/payment.model');
const {verifyAdmin,verifyUser}=require('../middlewares/auth');

/**
 * @swagger
 * /listPayMethods:
 *  get:
 *      summary: Obtener la lista de metodos de pago Usuarios
 *      tags: [metodosDePago]
 *      responses:
 *          '200':
 *              description: JSON con metodos de pago
 *              
 *                          
 */
paymentsRouter.get('/listPayMethods', verifyUser, (req,res) => {
    res.json(payMethodsGet());
});
/**
 * @swagger
 * /getPayMethods:
 *  get:
 *      summary: obtener la lista de metodos de pago con credenciales de administrador
 *      tags: [metodosDePago]
 *      responses:
 *          '200':
 *              description: JSON con metodos de pago
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/metodosDePago'
 */
paymentsRouter.get('/getPayMethods', verifyAdmin, (req,res) =>{
    res.json(payMethodsGet());
});
/**
 *@swagger
 * /createPayMethod:
 *  post:
 *      summary: Crear nuevo metodo de pago
 *      tags: [metodosDePago]
 *      responses:
 *          '200':
 *              description: JSON con metodos de pago creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/metodosDePago'
 *          '400':
 *              description: Error, debe ser administrador para crear un método de pago
 *                  
 */
paymentsRouter.post('/createPayMethod',verifyAdmin, (req,res) => {
    const method = createPayMethod(req.body.method);
    if (method){
        res.status(201).json({"Metodo de pago creado": method});
    }else {
        res.status(400).json({"ERROR":"debe ser administrador para crear un método de pago"});
    }
});

/**
 * @swagger
 * /deletePayMethod:
 *  delete:
 *      summary: borrar uno o más métodos de pago
 *      tags: [metodosDePago]
 *      requestBody:
 *          required: true
 *          content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          example:
 *                              id: 1
 *      responses:
 *          '200':
 *              description: Método de pago borrado
 *              
 *          '400':
 *              description: Error, no existe el método de pago
 */
paymentsRouter.delete('/deletePayMethod', verifyAdmin, (req,res) => {
    const isDeleted = deletePayMethod(req.body.id);
    console.log(isDeleted);
    if (isDeleted){
        res.status(200).json({"Metodo de pago borrado": isDeleted});
    }else{
        res.status(400).json({"ERROR":"No existe el metodo de pago"});
    }
});

/**
 * @swagger
 * /editPayMethod:
 *  put:
 *      summary: editar uno o más métodos de pago
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      example:
 *                          id: 2
 *                          method: paypal
 *      tags: [metodosDePago]
 *      responses:
 *          '200':
 *              description: Método de pago editado
 *              
 *          '400':
 *              description: Error, no se encuentra el método de pago
 */
paymentsRouter.put('/editPayMethod',(req,res) => {
    const {id,method} = req.body;
    if(id && method){ 
        const edited= editPayMethod(id,method);
        if( edited ){
            res.status(200).json({"Metodo de pago actualizado":
                {
                    "id": edited.id,
                    "metodo de pago": edited.method
                }
            });
        }else{
            res.status(404).json({"Metodo de pago no Encontrado":id})
        }

    }
});
/**
 * @swagger
 * tags:
 *  name: metodosDePago
 * components:
 *  schemas:
 *      metodosDePago:
 *          type: object
 *          required:
 *              -name
 *          properties:
 *              name:
 *                  type: string
 *          example:
 *              name: nequi
 */



module.exports={paymentsRouter};





