const express = require('express');
const productsRouter = express.Router();

const{getProducts,createProducts,updateProducts,deleteProducts,listProducts} = require ('../models/products.model');
const {verifyAdmin,verifyUser}=require('../middlewares/auth');

/**
 * @swagger
 * /getProducts:
 *  get:
 *      summary: Obtener la lista de productos con credenciales de admin
 *      tags: [Productos]
 *      responses:
 *          '200':
 *              description: JSON con productos
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

productsRouter.get('/getProducts', verifyAdmin, (req,res) => {
    res.json(getProducts());
});

/**
 * @swagger
 * /listProducts:
 *  get:
 *      summary: obtener la lista de metodos de pago con credenciales de usuario
 *      tags: [Productos]
 *      responses:
 *          '200':
 *              description: JSON con lista de productos disponibles
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 */
productsRouter.get('/listProducts', verifyUser, (req,res) => {
    res.json({"Esta es la lista de productos":listProducts()})
});

/**
 *@swagger
 * /createProduct:
 *  post:
 *      summary: Crear nuevo producto con credenciales de administrador
 *      tags: [Productos]
 *      responses:
 *          '200':
 *              description: JSON con producto creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/metodosDePago'
 *          '400':
 *              description: Error, debe ser administrador para crear un producto
 *                  
 */

productsRouter.post('/createProduct',verifyAdmin, (req,res) =>{
    const{name,price} = req.body;
    created = createProducts(name,price);
    if (created){
        res.status(201).json({"Producto creado" : {
        "id": created.id,
        "name":created.name,
        "price":created.price,
        "activo" : created.activo
        }})
    }else{
        res.status(404).json("llena todos los campos")
    }
});
/**
 * @swagger
 * /updateProduct:
 *  put:
 *      summary: editar uno o más métodos de pago
 *      tags: [Productos]
 *      responses:
 *          '200':
 *              description: Producto editado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          example:
 *                              id: 2
 *                              method: paypal
 *          '400':
 *              description: Error, no se encuentra el producto
 */

productsRouter.put('/updateProduct', verifyAdmin, (req,res) => {
    const {id, name, price, activo} = req.body;
    if (id && name && price && activo != undefined){
        updated = updateProducts(id, name, price, activo);
        if (updated){
        res.status(200).json({"producto actualizado":{"id":id,"name":name,"price":price,"activo": activo}});
        }else{
            res.status(404).json({"ERROR":"No se encuentra el producto"});
        }
    }else{
        res.status(400).json({"ERROR":"Completa todos los campos"});
    }
})
/**
 * @swagger
 * /deletePayMethod:
 *  delete:
 *      summary: borrar uno o más métodos de pago
 *      tags: [metodosDePago]
 *      responses:
 *          '200':
 *              description: Método de pago borrado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          example:
 *                              id: 2
 *          '400':
 *              description: Error, no existe el método de pago
 */

productsRouter.delete('/deleteProduct', verifyAdmin, (req,res) => {
    console.log(req.query);
    const {id} = req.query;
    if (id){
        deleted=deleteProducts(id);
        if (deleted){
            res.status(200).json({"Producto eliminado con exito":{"id eliminado" : id}});
        }else{
            res.status(404).json({"ERROR":"No se encuentra el producto"});
        }
    }else{
        res.status(400).json({"ERROR":"Completa los campos requeridos"});
    }   
})

module.exports={productsRouter};


