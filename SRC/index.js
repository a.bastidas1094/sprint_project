const express = require('express');
const app = express();
const basicAuth = require('express-basic-auth');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerOptions = require('./utils/swaggerOptions');

app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));


const {
    usuarioRouter
} = require('./routes/Users.routes');
const {
    productsRouter
} = require('./routes/products.routes');
const {
    paymentsRouter
} = require('./routes/payment.routes');
const {
    pedidosRouter
} = require('./routes/pedidos.routes');


app.use('/', usuarioRouter);

app.use('/', pedidosRouter);

app.use('/', productsRouter);

app.use('/', paymentsRouter);




app.listen(3000, () => {
    console.log("Escuchando desde el puerto 3000")
});
