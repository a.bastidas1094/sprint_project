# Sprint-Project 1: Api Restaurante Delilah Restó

En este Proyecto encontrarás un Api desarrollada en Express para el cliente hipotetico Delilah Restó. Cuenta con tres entidade: usuarios, productos y pedidos con las operaciones CRUD necesarias.


## Iniciando nuestra Api

Las siguientes instrucciones permiten obtener una copia del proyecto, para que funcione en tú equipo local, con el objetivo de realizar pruebas y desarrollos.


### Recursos

_Para desplegar el proyecto es necesario tener instalado_

```
node.js
```
- No es obligatorio, pero se recomienda el uso de [Visual Studio Code](https://code.visualstudio.com/docs) para modificar y desplegar el proyecto.

### Instalación

El primer paso es clonar el proyecto del repositorio en la carpeta local, para ello usaremos en nuestra terminal:

```
git clone: https://gitlab.com/a.bastidas1094/sprint-project
```

_Ubicados en la carpeta del archivo, ejecutamos:_

```
npm install
```

Con los pasos anteriores se instalaran los paquetes necesarios en nuestro equipo, para que el proyecto quede listo para el despliegue.


## Despliegue

Para realizar el despliegue debemos estar ubicados en la carpeta del proyecto y en una terminal ejecutaremos:

```
nodemon src/index.js
```
Al hacerlo (si no se presenta ningun error) se mostrará en la salida de la terminal un mensaje similar a este:

```
[nodemon] 2.0.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): .
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node src/index.js`
Escuchando desde el puerto 3000
```
El mensaje anterior indica el puerto en el cual se esta ejecutando el proyecto, para acceder a el puede usar cualquier navegador y entrar a la direccion [localhost:3000](http://localhost:3000/api-docs)


## Rutas habilitadas para el usuario

La documentación ya incluye los ejemplos de cuentas creadas, como la siguiente: 

```
name: "jose campo",
email: "jose@nada.com",
password: "1234",
phoneNumber: "1234567890",
address: "av 23 boulevar",
```


## Esta Api fue construida con:

* [Node.js](https://nodejs.org/es/docs/) 
* [Express](https://maven.apache.org/)
* [Swagger](https://swagger.io/docs/)


## GitLab e Email del autor 

* **Andry Yulissa Bastidas Granda** 
- [a.bastidas1094](https://gitlab.com/a.bastidas1094)
- *a.bastidas1094@gmail.com* 